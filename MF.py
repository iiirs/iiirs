import sys, os
import random
import math

# Compute inner product of two vector
def dot(X, Y):
	ret = 0
	for i in range(len(X)):
		ret += X[i] * Y[i]
	return ret

# Compute root mean square error of two series
def rmse(X, Y):
	err = 0;
	for i in range(len(X)):
		err += (X[i] - Y[i]) * (X[i] - Y[i])
	return math.sqrt(err / len(X))

class MF():
	def __init__(Self, MaxUid, MaxIid, Rank):
		Self.MaxUid = MaxUid
		Self.MaxIid = MaxIid
		Self.Rank = Rank
		Self.U = []	# User latent matrix
		Self.V = []	# Item latent matrix
		for i in range(MaxUid):
			Self.U.append([])
			for j in range(Rank):	# Assign each user a random initialized latent factors
				Self.U[i].append(random.random())
		for i in range(MaxIid):
			Self.V.append([])
			for j in range(Rank):	# Assign each item a random initialized latent factors
				Self.V[i].append(random.random())

	# Predict ratting
	# User: User id
	# Item: Item id
	def predict(Self, UserId:int, ItemId:int):
		# If We haven't seen this user or item before ...
		if(UserId < 0 or UserId > Self.MaxUid or ItemId < 0 or ItemId > Self.MaxIid):
			return 0

		# TODO: Compute the ratting of given user to given item base on latent factors
		return 0# What should be here ?

	# Train the model
	# X: list of (User, Item) pair
	# Y: list of rattings correspond to X
	# Epochs: Iterations to run
	def fit(Self, X, Y, LearnRate = 0.01, Epochs = 10, GeneralizationRate = 0.2):
		for epoch in range(Epochs):	# Iterate for a number of times
			for k in range(len(X)):	# For each training instance (User, Item)
				UserId = X[k][0]
				ItemId = X[k][1]
				RealRatting = Y[k]

				# Predict based on our current model
				PredictedRatting = Self.predict(UserId, ItemId)

				# TODO: Compute the error
				# Our prediction is "PredictedRatting", but the real ratting is "RealRatting", so the error is ...
				Error = 0# What should be here ?

				# Now, we have to update the latent factors to mitigate the error
				for i in range(Self.Rank):
					# TODO: Compute gradient of user latent factor
					du = 0# What should be here ?
					# TODO: Compute gradient of user latent factor
					dv = 0# What should be here ?

					# TODO: Update latent factors
					Self.U[UserId][i] = 0# What should be here ?
					Self.V[ItemId][i] = 0# What should be here ?

def main(argc:int, argv:list):
	if(argc < 3):
		print("TrainFile TestFile")

	# You can try different parameter
	rank = 10
	learnrate = 0.01
	generalization = 0.2
	epochs = 20

	mf = MF(2000, 2000, Rank = rank)

	# Prepare data
	X = []
	Y = []
	P = []
	TX = []
	TY = []
	PT = []
	with open(argv[1], 'r') as ftr:
		for line in ftr:
			tokens = line.split()
			X.append((int(tokens[0]), int(tokens[1])))
			Y.append(float(tokens[2]))
			P.append(0)
	with open(argv[2], 'r') as fts:
		for line in fts:
			tokens = line.split()
			TX.append((int(tokens[0]), int(tokens[1])))
			TY.append(float(tokens[2]))
			PT.append(0)
	
	# Start experiment
	for r in range(epochs):
		# In general Epochs should be more, but we use a step by step approach for easy inspection
		mf.fit(X, Y, Epochs = 1, LearnRate = learnrate, GeneralizationRate = generalization)

		# Compute train error
		for k in range(len(X)):	# For each training instance (User, Item)
			UserId = X[k][0]
			ItemId = X[k][1]
			P[k] = mf.predict(UserId, ItemId)
		trerr = rmse(P, Y)

		# Compute test error
		for k in range(len(TX)):	# For each training instance (User, Item)
			UserId = TX[k][0]
			ItemId = TX[k][1]
			PT[k] = mf.predict(UserId, ItemId)
		tserr = rmse(PT, TY)
		print('Epoch ' + str(r) + ': Train RMSE = ' + str(trerr) + ', Test RMSE = ' + str(tserr))

if (__name__ == '__main__'):
	main(len(sys.argv), sys.argv)
