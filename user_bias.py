import sys
import math

if len(sys.argv)!= (1+1):
	print('1: output file')
	exit(-1)
input = 'MovieLens.train'
test_file = 'MovieLens.test'
output = sys.argv[1]


### calculate user bias
bias = {}
with open(input,'r') as f:
	for line in f:
		words = line.split()
		user = int(words[0])
		rating = int(words[2])
		if user not in bias.keys():
			bias[user] = [rating,1]
		else:
			bias[user][0] += rating
			bias[user][1] += 1

### just for testing
count = 0
rmse = 0
g = open(output,'w')
with open(test_file,'r') as f:
	for line in f:
		words = line.split()
		user = int(words[0])
		pred = float(bias[user][0])/bias[user][1]
		g.write('%f\n' % pred)
g.close()		

		
